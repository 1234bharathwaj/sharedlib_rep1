def call(body) {

    def pipelineParams= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()

    node {

        stage('stge one'){
            client_name = pipelineParams.client    
            echo "this is stage one end ${client_name}"
}
}
}
